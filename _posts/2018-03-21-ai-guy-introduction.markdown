---
draft: false
categories:
- AI Guy
uuid: a0af8717-8d89-46e7-bf9d-564a950f3854
layout: post
title: AI Guy introduction
id: 807
tags: []
image: wp-content/uploads/2018/03/animal-2027043_1280.png
---

Hi, I'm the guy with the autoimmune disease and we have been reading tons of research and autoimmune related information with my girlfriend factasticgirl over the course of the last year - she is the scientist in the family, so do not expect anything fancy like in her posts:-). The AI Guy section will have more personal experience related stuff were I will try to share some of the knowledge that I gained from taming my immune system. Well, we learned a lot and I tried a few things that helped me to get better and have control over my crazy immune system.&nbsp; I'm&nbsp;still experiencing some hiccups on the way to a better health but overall I'm learning to tame this beast. Thinking about my immune system as a beast gone wild and the cause behind it helped me a lot. And taming the beast needs first and foremost the right mindset. So let's talk about that. First comes the shock of course. You are in your mid thirties and think you are super healthy because you have a plant based fat free diet and doing exercises. Then severe neck pain starts and you can't walk one day, ok I could walk but a 100 m distance took me 15 minutes because of my&nbsp;sacroiliac joint pains. Did I mentioned that I also built a six pack in my belly with&nbsp; just sleeping at night. Yeah that was real fun. Turns out I was cramping my body and abdominal muscles really good while sleeping and after a few weeks I had real nice six pack sitting at my belly. The problem was that the cramping was also taking place at my back and the muscles there were getting tenser by the day until I couldn't do my morning exercises anymore to stretch the cramped up muscles. A nice vicious cycle, heh? Then I could not get up in the mornings. Waking up and thinking, Am I paralyzed? I can't move my body! After a few minutes some movements and in a few hours managed to drag my body out of bed. These kind of shocks were becoming the daily routine, my orthopaedist still insisting on injections for my stiff muscles and than my blood infection indicator - CRP - shot up. Finally was sent to the rheumatologist and the rest is history. Yeah but back to the right mindset. I took quite a while to adapt the "right" mindset. What's the right mindset? Simple said, stay positive and approach things with a can do mentality. Even if the doctors say there is no cure and you will probably never get better, your health will probably deteriorate over time and so on. The one thing that helped was the right mindset. How I learned to have the right mindset? We can talk about it in the coming blog posts.

