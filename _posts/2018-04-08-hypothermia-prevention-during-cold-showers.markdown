---
draft: false
categories:
- AI Guy
uuid: 7feaff55-9aba-4757-a966-22b7a5ed8fd4
layout: post
title: Hypothermia Prevention During Cold Showers
id: 960
tags: []
image: wp-content/uploads/2018/04/animals-beak-birds-914463.jpg
---

## Our goal is to stay warm during and after the cold showers.
Body heat can be lost 25 times faster in cold water compared to cold air.
### What is Hypothermia? 
Its a physical condition that occurs when the body's core temperature falls to (35° C) or cooler.Even a mild case of hypothermia diminishes our physical and mental abilities, thus increasing the risk of accidents. Severe hypothermia may result in unconsciousness and possibly death.How quickly a person becomes hypothermic depends on a variety of factors, including personality, behavior, physical condition, clothing, and environmental factors. Older people, those under the influence of alcohol or drugs are particularly susceptible to hypothermia. Children and people with certain health disorders are also vulnerable. So are outdoor enthusiasts who spend time boating, fishing, swimming, hiking or skiing.According to [Minnesota Sea Grant](http://www.seagrant.umn.edu/coastal_communities/hypothermia) this are the survical times in cold water:[one\_third\_first]Water Temperature[/one\_third\_first][one\_third]Expected Time Before Exhaustion or Unconsciousness[/one\_third][one\_third\_last]Expected Time of Survival[/one\_third\_last] [one\_third\_first]3.3–10°[/one\_third\_first][one\_third]30 – 60 minutes[/one\_third][one\_third\_last]1 – 3 hours[/one\_third\_last] [one\_third\_first]10–15.6°[/one\_third\_first][one\_third]1 – 2 hours[/one\_third][one\_third\_last]1 – 6 hours[/one\_third\_last]Even if your water temperature is very cold you should not get a hypothermia in a few minutes of cold showering. If your body (cardiovascular system and the contradiction ability of your veins) is not ready and you stay in the cold shower too long your core temperature may fall a little and you could shiver and feel cold for an hour or more. So don't overdo it. ease into the cold showers. Don't force yourself and your body.