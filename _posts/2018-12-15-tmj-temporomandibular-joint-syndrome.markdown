---
draft: true
categories:
- Uncategorized
uuid: 15d614ba-e082-4faf-8a71-4ff8f629813c
layout: post
title: TMJ (Temporomandibular joint) Syndrome
id: 402
tags: []
---

The [Temporomandibular joint (TMJ)](https://www.nidcr.nih.gov/oralhealth/Topics/TMJ/TMJDisorders.htm) connects the lower jaw bone (the mandible) to the bone at the side of the skull on each side of the head with muscles controlling movements in three(up&down, forward&back, side to side) directions for coordination of actions such as talking, chewing, swallowing, and yawning.&nbsp;Temporomandibular Joint syndrome&nbsp;(TMJ), also known as TMD (TM-Disorder), causes symptoms such as jaw pain, limited ability to open the mouth, headaches, neck. Temporomandibular Disorders (TMD) are a complex and poorly understood set of conditions characterized by pain in the jaw joint and surrounding tissues and limitation in jaw movements

