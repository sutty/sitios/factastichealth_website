---
draft: true
categories:
- Some facts about
uuid: 5300a7b5-c54e-4b2b-998f-7f933e131f1c
layout: post
title: Autophagy
id: 911
tags: []
---

The 2016 Nobel Prize for Physiology or Medicine was awarded to [Dr. Yoshinori Ohsumi](https://www.nobelprize.org/nobel_prizes/medicine/laureates/2016/press.html) for his discoveries of mechanisms underlying the role of autophagy. Autophagy comes from two Greek words meaning "self eating". is a natural process for degrading and recycling cellular components. &nbsp;The word autophagy originates from two Greek words meaning “self-eating”.Nobel laureate Yoshinori Ohsumi’s work on mechanisms underlying autophagy — a fundamental process of degrading and recycling cellular components — has generated much interest in the science behind the biological process.
## Disruption of autophagy processes of the cell has been linked to Parkinson’s disease, type 2 diabetes.
The 2016 Nobel Prize for Physiology or Medicine was awarded to Japan’s Dr. Yoshinori Ohsumi for his discoveries of the underlying mechanisms of a physiological process called autophagy. Autophagy is a natural process by which the body degrades and recycles damaged cells, proteins and toxins. Autophagy comes from two Greek words,&nbsp;_auto_&nbsp;meaning “self” and&nbsp;_phagy_&nbsp;meaning “to eat.”