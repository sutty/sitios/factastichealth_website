---
draft: true
categories:
- Uncategorized
uuid: b4212dc9-5314-4ad7-90f6-5fdd070f9904
layout: post
title: Autoimmune Disorders – 2
id: 721
tags: []
---

The&nbsp;symptoms of the autoimmune disorders aren't visible and the average time for diagnosis of a serious autoimmune disease is [4.6 years where](https://www.aarda.org/wp-content/uploads/2016/12/AARDA-Do_you_know_your_family_AQ-DoubleSided.pdf)&nbsp;[46 percent of the patients](https://www.aarda.org/wp-content/uploads/2016/12/AARDA-Do_you_know_your_family_AQ-DoubleSided.pdf) were told initially that they were too concerned about their health. The&nbsp;