---
draft: false
categories:
- AI Guy
uuid: 7bea3ce2-1325-4940-a72b-f0dad4f08b10
layout: post
title: My Autoimmune Taming Morning Routine part 2 – Cold Showers in Detail
id: 916
tags: []
image: wp-content/uploads/2018/03/blair-fraser-21140-unsplash.jpg
---

### As mentioned in my [previous post](https://factastichealth.com/my-autoimmune-taming-morning-routine)&nbsp;my morning routine involves also taking cold showers, let me explain to you why this helped me a lot to tame the beast...
Personally, they help me to decrease my pain levels and the [inflammation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4034215/) my body is creating due to attacking itself because of my autoimmune disease. Cold showers can help you, in combination with breathing techniques and meditation, [decrease your inflammation levels.](https://www.ncbi.nlm.nih.gov/pubmed/22685240)They activate the sympathetic nervous system and [promote wakefulness](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4049052/), simply it's a good thing to wake up. Cold showers [activate your immune system](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4034215/) and regular exposure to cold showers [increases your&nbsp;immune system cells](https://www.ncbi.nlm.nih.gov/pubmed/8925815). They probably also help you to recover your [muscle power faster](https://www.researchgate.net/publication/278022513_Effects_of_cold_water_immersion_and_active_recovery_on_hemodynamics_and_recovery_of_muscle_strength_following_resistance_exercise) from a muscle&nbsp;soreness. If you want to activate your brown fat (brown adipose tissue), cold showers can help you also with that. Nice side effects are that your body generates heat and [burns calories](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3266793/).
### What about hypothermia and core temperature loss?
Body heat can be lost 25 times faster in cold water than in cold air.What is Hypothermia? It's a physical condition that occurs when the body's core temperature falls to 35° C or below.Even a mild case of hypothermia diminishes a victim's physical and mental abilities, thus increasing the risk of accidents. Severe hypothermia may result in unconsciousness and possibly death.Elderly people are particularly vulnerable to hypothermia.[If the&nbsp;Water Temperature is 3-10° C&nbsp;Expected Time Before Exhaustion or Unconsciousness is 30 to 60 minutes,&nbsp;at 10–15.6° its 1-2 hours.](http://www.seagrant.umn.edu/coastal_communities/hypothermia)&nbsp;