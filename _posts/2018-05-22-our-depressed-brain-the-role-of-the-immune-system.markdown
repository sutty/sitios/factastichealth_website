---
draft: false
categories:
- AI Guy
uuid: 97255842-86cd-4e50-b96b-565cdd68b0f3
layout: post
title: Our Depressed Brain - The Role of Our Immune System and Gut
id: 1107
tags: []
image: wp-content/uploads/2018/05/brain-3168269_1920.png
---

## Gastrointestinal depression is that even a thing?
Change or loss of mucosal tolerance causes an&nbsp;up or down regulation of key immune responses. These&nbsp;changes are related to problems&nbsp;with the gastrointestinal immune system. Proinflammatory chemicals are introduced into our body and result in the long term&nbsp;a chronic inflammatory conditions. This can cause autoimmune diseases, allergy, cancer, and depression.

[Some depression types can be&nbsp;traced back to&nbsp;a set of symptoms caused by&nbsp;a&nbsp;hyperactive&nbsp;immune system's inflammatory responses](https://www.ncbi.nlm.nih.gov/pubmed/12401468).

&nbsp;

 ![](https://factastichealth.com/wp-content/uploads/2018/05/Immune-system-chart.png)All the bacteria, viruses that cause an&nbsp;immunological stimulation in the gastrointestinal tract. This needs an ongoing requirement for a&nbsp;maintaining the consistency of our internal state in response to the&nbsp;alteration of the function of our biological systems, induced by external or internal stimuli. Proinflammatory cytokines produced by our body may&nbsp;highly contribute to a&nbsp;risk of developing depression (symptoms).

Signals to the brain are sent via several mechanisms by cytokines from our body's immune system. This can also&nbsp;cross&nbsp;the brain‐blood barrier via our bloodstream.

The&nbsp;cytokines activate our brain and induce&nbsp;behaviors of anxiety, depression, mood changes.

