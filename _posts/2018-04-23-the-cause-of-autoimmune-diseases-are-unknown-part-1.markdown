---
draft: false
categories:
- AI Guy
uuid: ee16c08c-2fca-4fb0-8442-cb92f5c904f1
layout: post
title: The Cause of Autoimmune Diseases are Unknown - Part 1
id: 1012
tags: []
image: wp-content/uploads/2018/04/allergens-2400px.png
---

And what you eat or supplement will not make any difference at all. If you hear such a statement from your physician or any other "expert", turn on your bullshit filter on and also be careful what other recommendations&nbsp;or&nbsp;solutions this person suggests. I bet his knowledge is ancient times and for a long time did not bother to read any research on the matter of autoimmune and nutrition. What is the reason that&nbsp;physicians&nbsp;hesitate so much to consider the possible connections between autoimmune diseases and what we are eating all day long? Maybe because in most medical schools they teach that&nbsp;fragments of dietary proteins are too large to leak from the intestine wall into the bloodstream and therefore are not the cause of inflammatory reactions in distant organs such as the joints, eyes even the heart. [Unfortunately,&nbsp;the opposite is true](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5440529/). As a&nbsp;sad result, you are given strong&nbsp;anti-inflammatory drugs to deal with the symptoms of the disease but not one of the potential major causes. Such a "relief" for us will often cause severe damage to our most crucial organs like the intestine, liver, bowel or the bone marrow in the form of bone&nbsp;marrow depression,&nbsp;bowel wall injury,&nbsp;liver inflammation or&nbsp;intestinal bleeding.[Part 2](https://factastichealth.com/the-cause-of-autoimmune-diseases-are-unknown-part-2) will be about the leaky gut. Thanks for inspiration for this series to [Clint Paddison](https://www.paddisonprogram.com/) ,[Dr. Michael Klapper](https://doctorklaper.com/answers/answers07)&nbsp;[Dr. John McDougall](https://www.drmcdougall.com/)

