---
draft: false
categories:
- AI Guy
uuid: f1e84add-2062-471d-ad35-1e7af1cc7a9b
layout: post
title: Gut Stress - Infection or Loss of Ecological Balance
id: 1077
tags: []
image: wp-content/uploads/2018/05/mental-health-2313426.png
---

## How is it possible that our gut gets into stress?
Our gut can be under stress from infections or a loss of ecological balance for example as a side effect of using antibiotics. Our body will try to restore the balance by decreasing&nbsp;[tryptophan](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5292609/).&nbsp;Tryptophan is also a precursor to&nbsp;[serotonin](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4728667/)&nbsp;a neurotransmitter that makes us "feel good".&nbsp;Tryptophan is&nbsp;ingested as food by some&nbsp;bacteria in our gut. In order our body can restore its gut balance it activates&nbsp;the enzyme indoleamine 2,3-dioxygenase and&nbsp;degrades serotonin and tryptophan. So no more food for&nbsp;bacteria that uses them. This can&nbsp;potentially cause depression and anxiety in us, degrade our memory - brain fog, disturbances in cognition, appetite, sleep and body temperature disturbances like&nbsp;cold extremities or&nbsp;fevers. Psychological and emotional stress can also trigger&nbsp;inflammation in our gut and brain. That's why mind-body exercises&nbsp;for managing stress are fundamental if we want to to get rid of inflammation and heal our gut. Without this, the healing won't last or wouldn't even&nbsp;take place in the first place. If you have a&nbsp;high-stress life or dealing with past/present traumas you should take extra care of your gut. &nbsp; &nbsp;