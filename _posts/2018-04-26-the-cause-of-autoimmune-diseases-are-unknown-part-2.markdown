---
draft: false
categories:
- AI Guy
uuid: 044564ed-3b8a-43d0-9569-19c2089ddcc7
layout: post
title: The Cause of Autoimmune Diseases are Unknown - Part 2
id: 1016
tags: []
image: wp-content/uploads/2018/04/kelly-sikkema-475112-unsplash.jpg
---

## What about the Leaky Gut?
The food choices we make can damage our gut directly with their physical or chemical properties if they are for example:&nbsp;acidic,&nbsp;greasy, digestive-intensive like meat or poor in nutrients. Indirectly the above-mentioned kind of foods&nbsp;imbalance the microbiome,&nbsp;the most important residents in our gut, billions and billions of microorganisms that live in the mucus layer of our intestines. They form one part of our gut functional barrier. Physicians ignoring the&nbsp;[effect of our diet and how that relates to inflammatory&nbsp;rheumatoid diseases like&nbsp;arthritis or&nbsp;ankylosing spondylitis](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5682732/) is very short-sighted and leads to symptom management&nbsp;rather than dealing with the&nbsp;cause of the disease. Through scientific research, it is known that in most people with inflamed, leaky gut conditions, protein fragments from foods and gut organisms enter the bloodstream after most meals. Our immune system forms antibodies in response to these foreign bodies and components of milk, wheat or egg&nbsp;proteins for example. The antibodies&nbsp;are formed&nbsp;within a very short time (hours) after consuming these foods. In [Part 3](https://factastichealth.com/the-cause-of-autoimmune-diseases-are-unknown-part-3) we will get into foods that are the culprits of a leaky gut.