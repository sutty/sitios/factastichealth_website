---
draft: false
categories:
- AI Guy
uuid: 9b81007f-00db-4394-a777-628c57fb4c13
layout: post
title: Night routine, do you have one?
id: 950
tags: []
image: wp-content/uploads/2018/04/john-fowler-330787-unsplash.jpg
---

## I have definitely&nbsp;one and it really helps me a lot also in dealing with the autoimmune disease.
The night is mostly for preparation and doing things offline as much as I can. Trying to avoid&nbsp;[blue light](https://www.scientificamerican.com/article/q-a-why-is-blue-light-before-bedtime-bad-for-sleep/). Like preparing the food and smoothies for the next day. Also the evening meditation&nbsp;before going to bed. That also really helps me with the [bruxism](https://en.wikipedia.org/wiki/Bruxism)&nbsp;I think due to leaving less for the brain to process while sleeping & dreaming. Mapping my top 3 priorities for the next day helps me to focus on them easier and get things done. Some brainstorming and thinking about my numero uno goal or problem that I try to solve before going to bed, primes up my mind. The most important rule is to go to bed at least 7 (or better 8) hours before the intended wake up time. &nbsp; &nbsp;